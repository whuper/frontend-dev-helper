﻿; (function () {

     /*   const page_host = window.location.host;
  
       if (typeof jQuery == 'undefined') {
            // console.log('No JQUERY');
       }
       const a_color = "background: rgb(248, 177, 173); color: rgb(0, 128, 0)"; */
     /*      window.clearTimeout(debugTimer);
     
          var debugTimer = setTimeout(() => {
     
          }, 300); */

     var debugTimer = null;

     // 使用 tryUntilSuccess 函数，第二个参数是重试间隔（毫秒）
     tryUntilSuccess(enableDevtoolFun, 1000,3);

     function enableDevtoolFun() {

          // 确保Vue已经加载
          if (Vue && Vue.version) {
               // 输出Vue版本
               console.log(`Vue version: ${Vue.version}, 浏览器调试窗的Vue选项卡激活时不要刷新页面`);

               const rootVueInstance = document.querySelector('body > *:first-child').__vue__;

               const contrRef = rootVueInstance.__proto__.constructor;
               contrRef.config.devtools = true;

               __VUE_DEVTOOLS_GLOBAL_HOOK__.emit('init', contrRef);

               window.clearTimeout(debugTimer);

          } else {
               console.warn('Vue devtool启动失败 #1 ');
          }
     }

     /**
      * 尝试执行函数直到成功，默认最多重试3次
      * @param {Function} fn 要执行的函数
      * @param {number} timeout 每次重试之间的延迟时间（毫秒）
      * @param {number} retriesLeft 剩余重试次数
      */
     function tryUntilSuccess(fn, timeout, retriesLeft = 3) {
          try {
               fn(); // 尝试执行函数
          } catch (error) {
               // console.warn('Vue devtool启动失败: ', error);
               if (retriesLeft > 0) {
                    // 如果还有重试次数，设置延迟并减少剩余重试次数
                    debugTimer = setTimeout(() => tryUntilSuccess(fn, timeout, retriesLeft - 1), timeout);
               } else {
                    // 如果已经没有重试次数，不再重试
                    // console.warn("重试次数已用尽，无法执行函数");
               }
          }
     }


     function debounce(fn, wait) {
          var timer
          return function () {
               if (timer !== null) {
                    window.clearTimeout(timer);
               }
               timer = window.setTimeout(fn, wait);
          }
     }


})();
