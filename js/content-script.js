﻿(function () {
	//console.log('hello ! 这是 content-script！');
})();


document.addEventListener('DOMContentLoaded', function () {
	// 注入自定义JS
	// injectCustomJs();
});

// 当一个资源及其依赖资源已完成加载时，将触发 load 事件, 比DOMContentLoaded更晚一些
/* window.addEventListener('load', () => {

}); */
window.addEventListener('load',async () => {
	// 注入自定义JS
	injectCustomJs();
  // 检查调试窗口是否打开	
	// const devToolsOpen = isDevToolsOpen();	
  // if (devToolsOpen) {
    // 如果调试窗口打开，关闭Vue选项卡
    // closeVueDevTools();
  // }
});

// 检查调试窗口是否打开
function isDevToolsOpen() {
  // 检查是否有inspect标志存在
  const isOpen = chrome.runtime && chrome.runtime.inspect && chrome.runtime.inspect.isOpen;

  return isOpen;
}

// 关闭Vue选项卡
function closeVueDevTools() {
	// console.warn('closeVueTab: ');

	//
	chrome.runtime.sendMessage({ closeVueTab: true });
}


// 向页面注入JS
function injectCustomJs(jsPath) {
	jsPath = jsPath || 'js/inject.js';
	var temp = document.createElement('script');
	temp.setAttribute('type', 'text/javascript');
	// 获得的地址类似：chrome-extension://ihcokhadfjfchaeagdoclpnjdiokfakg/js/inject.js
	// temp.src = chrome.extension.getURL(jsPath);
	temp.src = chrome.runtime.getURL(jsPath)
	/* 	temp.onload = function () {
			// 放在页面不好看，执行完后移除掉
			this.parentNode.removeChild(this);
		}; */
	// document.body.appendChild(temp);

	var html = document.documentElement;
	html.insertBefore(temp, html.firstChild);

}

